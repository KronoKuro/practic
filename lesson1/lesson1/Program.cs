﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lesson1
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("Для расчета квадратного уравнения введите а, b, c");
            Console.WriteLine("Введите а:");
            var a = Console.ReadLine();
            double resa;
            while (!Double.TryParse(a, out resa) || resa <= 0)
            {
                Console.WriteLine("Введите а которое не будет символом и будет больше 0");
                Console.WriteLine("Введите а:");
                a = Console.ReadLine();
            }
            Console.WriteLine("Введите b:");
            var b = Console.ReadLine();
            double resb = ReadNumberFromConsole(b);
            Console.WriteLine("Введите c:");
            var c = Console.ReadLine();
            double resc = ReadNumberFromConsole(c);
            double d = (resb * resb) - 4 * resa * resc;
            double x1;
            double x2;
            if (d > 0)
            {
                x1 = ((-resb) + Math.Sqrt(d)) / (2 * resa);
                x2 = ((-resb) - Math.Sqrt(d)) / (2 * resa);
                Console.WriteLine($"Квадратный корни уравнения равны x1 = {x1}, x2 = {x2}");
            }
            else if (d == 0)
            {
                x1 = -resb / (2 * resa);
                x2 = x1;
            }
            else
            {
                Console.WriteLine("Корней нет");
            }
            Console.ReadKey();
        }

        static double ReadNumberFromConsole(string s)
        {
            double result;
            while (!Double.TryParse(s, out result))
            {
                Console.WriteLine("Введите число,  не символом");
                s = Console.ReadLine();
            }
            return result;
        }
    }
}
