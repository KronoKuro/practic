﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;

namespace lesson2
{
    public class Program
    {
        public static void Main(string[] args)
        {
            string text = File.ReadAllText(@"C:\1.txt"); //считываем данные из 1 txt 
            string textTwo = File.ReadAllText(@"C:\2.txt"); //считываем данные из 2 txt 
            string textThree = File.ReadAllText(@"C:\3.txt"); //считываем данные из 3 txt 
            CountUniqueWord(text, textTwo, textThree);

            Console.ReadKey();
        }

        public static void CountUniqueWord(string text, string textTwo, string textThree)
        {
            string[] split = text.Split(new[] { ' ', ',', ':', '?', '!' }, StringSplitOptions.RemoveEmptyEntries);
            string[] split2 = textTwo.Split(new[] { ' ', ',', ':', '?', '!' }, StringSplitOptions.RemoveEmptyEntries);
            string[] split3 = textThree.Split(new[] { ' ', ',', ':', '?', '!' }, StringSplitOptions.RemoveEmptyEntries);
            string[] allSplit = split.Concat(split2).ToArray().Concat(split3).ToArray(); ;
            List<string[]> namesList = new List<string[]> { split, split2, split3 };
            namesList.Distinct();
            List<string> mas = allSplit.ToList();
            mas.Distinct();
            Console.WriteLine(split.Distinct().ToList().Count());
        }
    }
}